//
//  Miboton.swift
//  CICE_PRACTICA_18_iOS_COREDATA_PUSH
//
//  Created by Diego Angel Fernandez Garcia on 25/04/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit

@IBDesignable class Miboton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    @IBInspectable var border: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = border
            
        }
    }
    @IBInspectable var backgroundImageColor: UIColor = UIColor.init(red: 0, green: 122/255.0, blue: 255/255.0, alpha: 1) {
        didSet {
            self.backgroundColor = backgroundImageColor
        }
    }
    
}
